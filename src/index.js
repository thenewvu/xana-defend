'use strict';

const _ = require('lodash');

/**
 * Make an object defensible.
 * @param {Object} obj
 * @returns {Proxy}
 */
function defend(obj) {
	return new Proxy(obj, {
		get: function (target, property) {
			if (!_.has(target, property)) {
				throw new ReferenceError(
					`Property "${property}" does not exist.`
				);
			}
			if (_.isPlainObject(target[property])) {
				return defend(target[property]);
			} else {
				return target[property];
			}
		},
		set: function(target, property) {
			throw new Error(
				`Property "${property}" is read-only.`
			);
		}
	});
}

module.exports = defend;
